import Vue from 'vue';
import Skeleton from 'vue-loading-skeleton';

import App from './App.vue';

import '@/assets/css/all.sass';
import './registerServiceWorker';

Vue.use(Skeleton);
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
