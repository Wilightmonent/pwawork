// src/service-worker.js
// 設置緩存的前綴語後綴詞
workbox.core.setCacheNameDetails({
  prefix: 'browse-exp',
  suffix: 'v1.0.0',
});

/**
 * 讓 service worker 盡快更新與獲取頁面
 * 這兩個通常都是一起設置的
 */
workbox.core.skipWaiting();
workbox.core.clientsClaim();

/**
 * vue-cli 3.0 通過 workbox-webpack-plugin 來實現相關功能，我們需要加入
 * 下面這段語法來預緩存 dist(打包後) 下的所有檔案
 */
workbox.precaching.precacheAndRoute(self.__precacheManifest || []);

/**
 * 用來緩存 api 的方法，
 * 第一個參數是 api 的位置，可以是檔名也可以是路徑，這裡寫成 regexp
 * 第二個參數是處理方式，NetworkFirst() 會以網路優先，如果沒有網路
 * 才會讀取先前緩存的 respond
 */
workbox.routing.registerRoute(
  new RegExp('https://jsonplaceholder\\.typicode\\.com.*/'),
  new workbox.strategies.NetworkFirst()
)

/**
 * 接受 notification 的事件
 */
self.addEventListener('push', (event) => {
  const title = 'Get Started With Workbox';
  const options = {
    body: event.data.text()
  };
  event.waitUntil(self.registration.showNotification(title, options))
})