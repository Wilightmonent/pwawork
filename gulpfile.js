const gulp = require('gulp')
const git = require('gulp-git')
const fs = require('fs')
const path = require('path')
const WHITELIST = ['node_modules', '.git']

gulp.task('pull', (cb) => {
    git.pull('origin', 'master', (err) => {
        if (err) throw err
        cb()
    })
})

gulp.task('add', () => {
    const list = fs.readdirSync('./')
        .filter((file) => !WHITELIST.includes(file))
        .map((fileName) => path.join(path.resolve(), fileName))
    return gulp.src(list)
        .pipe(git.add())
})

gulp.task('commit', () => {
    const list = fs.readdirSync('./')
        .filter((file) => !WHITELIST.includes(file))
        .map((fileName) => path.join(path.resolve(), fileName))
    return gulp.src(list)
        .pipe(git.commit('commit from gulp'))
})

gulp.task('push', (cb) => {
    git.push('origin', 'master', {args: " -f"}, (err) => {
        if (err) throw err
        cb()
    })
})

gulp.task('git', gulp.series('add', 'commit', 'pull', 'push'))